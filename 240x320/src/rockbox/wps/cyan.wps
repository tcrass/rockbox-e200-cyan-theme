# => Copyright <=
# cyan by me@tcrass.de
# derived from Widecabbie by Jonas H�ggqvist
# derived from cabbie 2.0 default for the Sansa e200 Series by Marc Guay
# derived from cabbie 2.0 default (C) 2007, Johannes Voggenthaler (Zinc Alloy)
# Licensed under CC-BY-SA 3.0

# --- Main setup ------------------------------------------------------

# Enable status bar
%we

# --- Load albumart, if available ---
%Cl(0,0,112,112,c,c)

# --- Font definitions ---

# Font #2: a little smaller than default
%Fl(2,16-Adobe-Helvetica-Bold.fnt)

# --- Images/Icon strips ---

# Big volume meter
%xl(V,vm-big-$RES.bmp,0,0,25)

# Peak meter background
%xl(M,pm-bg-$RES.bmp,0,0)

# Big peak meter background
%xl(B,pm-big-bg-$RES.bmp,0,0)

# Progress bar background
%xl(P,pb-bg-$RES.bmp,0,0)


# --- Header ----------------------------------------------------------

%V(0,0,-,21,-) %Vg(ffe080,ffc000,000000)
%Vs(gradient)%s%al%Sx(Now Playing):%ar[%pp/%pe]


# --- Technical track info --------------------------------------------

# --- Info panel ---

%V(6,27,-6,130,-)
%x(x,info-panel-$RES.bmp,0,0)

# --- Info text ---

%V(14,36,60,80,2) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
%Vs(gradient)%al%fc
%Vs(gradient)%al%fk kHz
%Vs(gradient)
%Vs(gradient)Vol:
%Vs(gradient)%pv dB
%Vs(gradient)


# --- Artistic track info ---------------------------------------------

%V(0,162,-,66,-)
%s%ac%?id<%id|%?d(1)<%d(1)|%(root%)>>:
%s%ac%?it<%it|%fn>
%s%ac(%?ia<%ia|%?d(2)<%d(2)|%(root%)>> %?iy<%iy|>)


# --- Bottom panel ----------------------------------------------------

%V(0,248,-,50,-)
%x(b,bottom-panel-$RES.bmp,0,0)

# --- Progress text ---

%V(8,254,72,21,-) %Vb(000000) %Vf(40ff00) %Vg(000000,000000,40ff00)
%Vs(gradient)%al%pc
%V(80,254,-80,21,-) %Vb(000000) %Vf(ffd040) %Vg(000000,000000,ffd040)
%Vs(gradient)%ac%pt
%V(-80,254,-8,21,-) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
%Vs(gradient)%ar-%pr


# --- Progress bar ---

%V(8,276,-8,16,-)
%pb(0,0,224,16,pb-$RES.bmp,backdrop,P)


# --- Bottom viewport -------------------------------------------------

%V(0,298,-,-,2)

#--- Background ---

%x(s,sb-bg-$RES.bmp,0,0)

# === Conditional viewports ===========================================

# Show the viewports (defined below) depending on whether
# volume is chaning...
#
# v: Volume change
#
# ...whether album art is available...
#
# z: Album art image
# m: Peak meter
#
# ...or not...
#
# b: Big peak meter
#
# ...and whether or not we have a next track
#
# n: Next track info


%?mv<%Vd(v)|%?C<%Vd(z)%Vd(m)|%Vd(b)>>
%?Fn<%Vd(n)|>


# --- Volume change (displayed on volume change) ----------------------

%Vl(v,80,36,153,112,-) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%?pv<%xd(Va)|%xd(Vb)|%xd(Vc)|%xd(Vd)|%xd(Ve)|%xd(Vf)|%xd(Vg)|%xd(Vh)|%xd(Vi)|%xd(Vj)|%xd(Vk)|%xd(Vl)|%xd(Vm)|%xd(Vn)|%xd(Vo)|%xd(Vp)|%xd(Vq)|%xd(Vr)|%xd(Vs)|%xd(Vt)|%xd(Vu)|%xd(Vv)|%xd(Vw)|%xd(Vx)|%xd(Vy)>
%Vl(b,80,36,144,112,-) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)


# --- Big peak meter (displayed if album art is not available ---------

# Hack! Using gradient since setting bg color does not have any effect!
# And using multiple empty lines just to ensure backdrop doesn't shine through!
%Vl(b,80,36,153,112,-) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
# Peak meters
%pL(36,1,32,110,pm-big-$RES.bmp,vertical,backdrop,B)
%pR(80,1,32,110,pm-big-$RES.bmp,vertical,backdrop,B)


# --- Album art -------------------------------------------------------

# Hack! Using gradient since setting bg color does not have any effect!
# And using multiple empty lines just to ensure backdrop doesn't shine through!
%Vl(z,80,36,124,112,-) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
# The actual albumart
%Cd


# --- Peak meter ------------------------------------------------------

# Hack! Using gradient since setting bg color does not have any effect!
# And using multiple empty lines just to ensure backdrop doesn't shine through!
%Vl(m,204,36,29,112,-) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
# The peak meter
%pL(0,1,6,110,pm-$RES.bmp,vertical,backdrop,M)
%pR(12,1,6,110,pm-$RES.bmp,vertical,backdrop,M)


# --- Next track info -------------------------------------------------

%Vl(n,0,228,-,16,2)
%s%ac%?Ia<%Sx(Next:) %Ia|%?D(2)<%Sx(Next:) %D(2)|%Sx(Next:) %(root%)>> - %?It<%It|%Fn>
