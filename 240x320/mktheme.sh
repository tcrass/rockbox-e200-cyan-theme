#!/bin/bash

RES=240x320x24

echo Removing old files...
rm *.png
rm *.bmp
rm rockbox/wps/cyan/*

./mkimg.sh

echo Re-linking rockbox folder...
rm .rockbox
ln -s rockbox .rockbox

echo Creating theme folders...
mkdir -p rockbox/backdrops/
mkdir -p rockbox/wps/cyan/

echo Copying theme files...
sed 's/\$RES/'$RES'/' <src/rockbox/wps/cyan.sbs >rockbox/wps/cyan.sbs
sed 's/\$RES/'$RES'/' <src/rockbox/wps/cyan.wps >rockbox/wps/cyan.wps

echo Creating bitmaps...
for path in *.svg; do
    name=$(basename $path .svg)
    inkscape --export-type="png" "$path"
done

echo Copying bitmaps...
cp cyan.bmp rockbox/backdrops/cyan.bmp
convert sb-bg.png rockbox/wps/cyan/sb-bg-$RES.bmp
convert info-panel.png rockbox/wps/cyan/info-panel-$RES.bmp
convert vm-big.png rockbox/wps/cyan/vm-big-$RES.bmp
convert pm.png rockbox/wps/cyan/pm-$RES.bmp
convert pm-bg.png rockbox/wps/cyan/pm-bg-$RES.bmp
convert pm-big.png rockbox/wps/cyan/pm-big-$RES.bmp
convert pm-big-bg.png rockbox/wps/cyan/pm-big-bg-$RES.bmp
convert bottom-panel.png rockbox/wps/cyan/bottom-panel-$RES.bmp
convert pb.png rockbox/wps/cyan/pb-$RES.bmp
convert pb-bg.png rockbox/wps/cyan/pb-bg-$RES.bmp
convert vm.png rockbox/wps/cyan/vm-$RES.bmp
convert hold.png rockbox/wps/cyan/hold-$RES.bmp
convert repeat.png rockbox/wps/cyan/repeat-$RES.bmp
convert shuffle.png rockbox/wps/cyan/shuffle-$RES.bmp
convert playmode.png rockbox/wps/cyan/playmode-$RES.bmp
convert battery.png rockbox/wps/cyan/battery-$RES.bmp
convert battery-charging.png rockbox/wps/cyan/battery-charging-$RES.bmp

echo Packing theme archive...
rm cyan.zip
zip -r cyan .rockbox -x \*~
cp cyan.zip cyan-$RES.zip
