#!/bin/bash

echo Creating backdrop...

convert ../common/src/default_blue.jpg \
  -rotate 90 -scale 176x220! \
  -modulate 175,90,90 \
  \( \
    -size 176x220 xc:#ffffff00 \
  \) -compose Screen -composite \
  cyan.bmp
