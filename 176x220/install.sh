#!/bin/bash

dev=/dev/sde

./mktheme.sh

echo Installing theme on simulator...
unzip -o cyan -d git/build-dir/simdisk

echo Prepraring theme installation on player...
sudo umount $dev
rm -rf DEVICE
mkdir DEVICE

echo Trying to mount Player...
sudo mount -o utf8,shortname=winnt,iocharset=iso8859-1,umask=000 "$dev" DEVICE

if [ -e DEVICE/.rockbox ]; then
  echo Installing theme on player...
  unzip -o cyan -d DEVICE
  sync
  echo Unmounting player...
  sudo umount DEVICE
else
  echo No player found, hence skipping installation!
fi

echo Done.
