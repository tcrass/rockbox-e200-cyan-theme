# => Copyright <=
# cyan by me@tcrass.de
# derived from Widecabbie by Jonas H�ggqvist
# derived from cabbie 2.0 default for the Sansa e200 Series by Marc Guay
# derived from cabbie 2.0 default (C) 2007, Johannes Voggenthaler (Zinc Alloy)
# Licensed under CC-BY-SA 3.0

# --- Main setup ------------------------------------------------------

# Enable status bar
%we

# --- Load albumart, if available ---
%Cl(26,0,72,72,c,c)

# --- Font definitions ---

# Font #2: a little smaller than default
%Fl(2,14-Adobe-Helvetica-Bold.fnt)

# --- Images/Icon strips ---

%xl(V,vm-big-176x220x24.bmp,0,0,25)
%xl(P,pm-bg-176x220x24.bmp,0,0)
%xl(B,pb-bg-176x220x24.bmp,0,0)


# --- Header ----------------------------------------------------------

%V(0,0,-,18,-) %Vg(ffe080,ffc000,000000)
%Vs(gradient)%s%al%Sx(Now Playing):%ar[%pp/%pe]


# --- Technical track details -----------------------------------------

# --- Info panel ---

%V(4,21,-4,84,-)
%x(x,info-panel-176x220x24.bmp,0,0)

# --- Info text ---

%V(12,28,56,56,2) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
%Vs(gradient)%al%fc
%Vs(gradient)%al%fk kHz
%Vs(gradient)Vol:
%Vs(gradient)%pv dB


# --- Artistic track info ---------------------------------------------

%V(0,107,-,56,-)
%s%ac%?id<%id|%?d(1)<%d(1)|%(root%)>>:
%s%ac%?it<%it|%fn>
%s%ac(%?ia<%ia|%?d(2)<%d(2)|%(root%)>> %?iy<%iy|>)


# --- Bottom panel ----------------------------------------------------

%V(0,171,-,33,-)
%x(b,bottom-panel-176x220x24.bmp,0,0)

# --- Progress text ---
#%V(4,174,-4,18,-) %Vf(ffffff) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
#%Vs(gradient)%al%pc%ac%pt%ar-%pr

%V(4,174,54,21,-) %Vb(000000) %Vf(40ff00) %Vg(000000,000000,40ff00)
%Vs(gradient)%al%pc
%V(58,174,-58,21,-) %Vb(000000) %Vf(ffd040) %Vg(000000,000000,ffd040)
%Vs(gradient)%ac%pt
%V(-58,174,-4,21,-) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
%Vs(gradient)%ar-%pr



# --- Progress bar ---

%V(3,192,-3,10,-)
%pb(0,0,170,10,pb-176x220x24.bmp,backdrop,B)


# === Conditional viewports ===========================================

# Show the viewports (defined below) depending on whether
# volume is chaning...
#
# v: Volume change
#
# ...whether album art is available...
#
# p; Peak meter
# z: Album art image
#
# ...and whether or not we have a next track
#
# n: Next track info


%?mv<%Vd(v)|%?C<%Vd(z)|%Vd(p)>>
%?Fn<%Vd(n)|>


# --- Volume change (displayed on volume change) ----------------------

%Vl(v,68,27,100,72,-)
%?pv<%xd(Va)|%xd(Vb)|%xd(Vc)|%xd(Vd)|%xd(Ve)|%xd(Vf)|%xd(Vg)|%xd(Vh)|%xd(Vi)|%xd(Vj)|%xd(Vk)|%xd(Vl)|%xd(Vm)|%xd(Vn)|%xd(Vo)|%xd(Vp)|%xd(Vq)|%xd(Vr)|%xd(Vs)|%xd(Vt)|%xd(Vu)|%xd(Vv)|%xd(Vw)|%xd(Vx)|%xd(Vy)>


# --- Peak meter (displayed if album art is not available -------------

# Hack! Using gradient since setting bg color does not have any effect!
# And using multiple empty lines just to ensure backdrop doesn't shine through!
%Vl(p,68,27,100,72,-) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
# Peak meters
%pL(36,2,24,69,pm-176x220x24.bmp,vertical,backdrop,P)
%pR(70,2,24,69,pm-176x220x24.bmp,vertical,backdrop,P)


# --- Album art -------------------------------------------------------

# Hack! Using gradient since setting bg color does not have any effect!
# And using multiple empty lines just to ensure backdrop doesn't shine through!
%Vl(z,68,27,100,72,-) %Vb(000000) %Vf(ffffff) %Vg(000000,000000,ffffff)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
%Vs(gradient)
# The actual albumart
%Cd


# --- Next track info -------------------------------------------------

%Vl(n,0,162,-,9,0)
%s%ac%?Ia<%Sx(Next:) %Ia|%?D(2)<%Sx(Next:) %D(2)|%Sx(Next:) %(root%)>> - %?It<%It|%Fn>
